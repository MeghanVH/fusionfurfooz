import { Injectable } from '@angular/core';
import { Flobette } from '../models/flobette';
import { BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FlobetteServiceService {
  private url: string = "https://furfooz.somee.com/api/buvette";
  context : BehaviorSubject<Flobette[]>

  constructor(private http : HttpClient) {
    this.context = new BehaviorSubject<Flobette[]>(null);
    this.refresh();
   }

  refresh(){
    this.http.get<Flobette[]>(this.url).subscribe(data=> 
     {this.context.next(data)});
  }
}
