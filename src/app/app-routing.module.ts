import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: () => import('./components/home/home.module').then( m => m.HomeModule)
  },
  {
    path: 'sortedpoilist',
    loadChildren: () => import('./components/generics/sortedpoilist/sortedpoilist.module').then( m => m.SortedpoilistPageModule)
  },
  {
    path: 'genericpoi',
    loadChildren: () => import('./components/generics/genericpoi/genericpoi.module').then( m => m.GenericpoiPageModule)
  },
  {
    path: 'mapmenu',
    loadChildren: () => import('./components/generics/mapmenu/mapmenu.module').then( m => m.MapmenuPageModule)
  },
  {
    path: 'settings',
    loadChildren: () => import('./components/settings/settings.module').then( m => m.SettingsPageModule)
  },
  { path: 'mission', loadChildren: () => import('./components/mission/mission.module').then(m => m.MissionModule) },
  {
    path: 'map',
    loadChildren : () => import('./components/map/map.module').then(m => m.MapPageModule)
  },
  { path: 'infos', loadChildren: () => import('./components/prices-ninfos/prices-ninfos.module').then(m => m.PricesNinfosModule) },
  { path: 'flobette', loadChildren: () => import('./components/flobette/flobette.module').then(m => m.FlobetteModule) },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
